from datetime import date


def round_to_nearest_half(number):
    return round(number * 2) / 2


def modified_calculate_warmup_weights(target_weight):
    warmup_percentages = [0.4, 0.6, 0.8, 0.9]
    warmup_weights = [round_to_nearest_half(target_weight * percentage) for percentage in warmup_percentages]
    return warmup_weights


def switched_get_current_stage(start_date, today):
    strength_duration = 12  # weeks
    transition_duration = 2  # weeks
    hypertrophy_duration = 12  # weeks

    cycle_duration = hypertrophy_duration + transition_duration + strength_duration + transition_duration

    weeks_elapsed = (today - start_date).days // 7
    weeks_elapsed_without_deloads = weeks_elapsed - (weeks_elapsed // 4)

    current_cycle_week = weeks_elapsed_without_deloads % cycle_duration

    if 0 <= current_cycle_week < hypertrophy_duration:
        return "hypertrophy", hypertrophy_duration - current_cycle_week
    elif hypertrophy_duration <= current_cycle_week < hypertrophy_duration + transition_duration:
        return "transition", hypertrophy_duration + transition_duration - current_cycle_week
    elif hypertrophy_duration + transition_duration <= current_cycle_week < cycle_duration:
        return "strength", cycle_duration - current_cycle_week
    else:
        return "transition", cycle_duration - current_cycle_week

def main():
    start_date = date(2023, 1, 1)
    today = date.today()

    current_stage, weeks_until_next_stage = switched_get_current_stage(start_date, today)
    print(f"Current stage is: {current_stage}")
    print(f"Weeks left in this stage: {weeks_until_next_stage}")

    if current_stage == "strength":
        print("\nRecommended warmup rep ranges for strength: 10, 8, 5, 3")
    elif current_stage == "hypertrophy":
        print("\nRecommended warmup rep ranges for hypertrophy: 12, 11, 10, 9")

    num_exercises = int(input("Enter the number of exercises: "))
    exercises = []

    for i in range(num_exercises):
        exercise_name = input(f"Enter the name of exercise {i + 1}: ")
        target_weight = float(input(f"Enter your target weight in KG for 5 reps of {exercise_name}: "))
        exercises.append((exercise_name, target_weight))

    print("\nRecommended warm-up set weights:")
    for exercise_name, target_weight in exercises:
        warmup_weights = modified_calculate_warmup_weights(target_weight)
        print(f"\n{exercise_name}:")
        for i, weight in enumerate(warmup_weights, start=1):
            print(f"  Set {i}: {weight} KG")


if __name__ == "__main__":
    main()
