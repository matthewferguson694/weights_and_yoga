#! /usr/bin/python3

import subprocess
import time
import datetime
import os
from datetime import date, timedelta

__author__ = 'mferguson'

# Exercises
# https://www.verywellfit.com/the-plank-exercise-3120068
# https://www.verywellfit.com/how-to-do-a-hollow-body-hold-techniques-benefits-variations-5079591
# https://www.healthline.com/health/exercise-fitness/dead-bug-exercise#variations
# Shoulders working exercises
shoulders_work_1 = {"Exercise": "Behind Neck Barbell Press",
                    "Instruction": "Arms in tight, life-line grip to start", "Weight": "43kg",
                    "Reps": "3", "Muscle group": "Shoulders"}

# Shoulders supplemental exercises
shoulders_supplemental_1 = {"Exercise": "Dual frontal raise", "Instruction": "hold like hammers\n", "Weight": "10kg",
                            "Reps": "15", "Muscle group": "Shoulders"}
shoulders_supplemental_2 = {"Exercise": "One handed dumbbell bent over curl", "Instruction": "\n", "Weight": "15kg",
                            "Reps": "8"}
shoulders_supplemental_3 = {"Exercise": "Bent-Over reverse fly",
                            "Instruction": "Retract shoulders, straight arms\n",
                            "Weight": "5kg, 7.5kg, 10kg, 12.5kg, 17.5kg",
                            "\nReps": "15, 14, 12, 10, 10, 10",
                            "Muscle group": "Shoulders"}
shoulders_supplemental_4 = {"Exercise": "Lateral raise",
                            "Instruction": "\n1. Neutral grip, with thumbs towards weight\n2. Shoulders retracted\n"
                            "3. Keep hands flat, don't raise pinky\n4. Focus on technique\n"
                            "5. Tense the glutes to create excessive movement\n"
                            "6. During lift stay in 15o to 30o rotation forward\n",
                            "Weight": "5kg", "Reps": "10, 11, 12, 13, 14", "Muscle group": "Shoulders"}
shoulders_supplemental_5 = {"Exercise": "Bent-Over rear delt fly, barbell grip, focus on rear delt activation",
                            "Instruction": "Retract shoulders, straight arms\n", "Weight": "5kg, 7.5kg, 10kg, 12.5kg",
                            "\nReps": "15, 14, 12, 10, 10",
                            "Muscle group": "Shoulders"}

# Shoulders stretches
shoulders_stretch_1 = {"Exercise": "Resistance band pull apart", "Weight": "BodyWeight", "Reps": "20"}
shoulders_stretch_2 = {"Exercise": "Resistance band external rotator cuff extension", "Weight": "BodyWeight",
                       "Reps": "25"}
shoulders_stretch_3 = {"Exercise": "Resistance band internal rotator cuff extension", "Weight": "BodyWeight",
                       "Reps": "25"}

# Shoulders warmup
shoulders_warmup_1 = {"Exercise": "Behind Neck Barbell Press",
                      "Instruction": "Stop behind the mid head, to protect scapula\n"
                      "Arms in tight, life-line grip to start", "Weight": "17kg, 25kg, 33kg, 38kg",
                      "Reps": "10, 8, 6, 3", "Muscle group": "Shoulders"}
shoulders_warmup_2 = {"Exercise": "Bent-Over reverse fly",
                      "Instruction": "Retract shoulders, straight arms\n", "Weight": "7.5kg, 10kg, 12.5kg",
                      "\nReps": "12-8",
                      "Muscle group": "Shoulders"}

# Forearms working exercises
forearm_work_1 = {"Exercise": "Reverse grip Ez curl",
                  "Instruction": "Don't use thumbs for grip\nDo top half of exercise\nThumbs up",
                  "Weight": "6kg", "Reps": "9", "Muscle group": "Forearms"}
forearm_work_2 = {"Exercise": "Reverse barbell curl",
                  "Instruction": "Don't use thumbs for grip\nDo top half of exercise\nThumbs up",
                  "Weight": "6kg", "Reps": "9", "Muscle group": "Forearms"}
forearm_work_2 = {"Exercise": "Suitcse lift", "Instruction": "\n", "Weight": "22kg", "Reps": "30 seconds",
                  "Muscle group": "Forearms"}

# Forearms supplemental exercises
forearm_supplemental_1 = {"Exercise": "Forearm bench support palms down",
                          "Instruction": "\nLeft first, count to 30 inbetween",
                          "Weight": "5kg", "Reps": "14", "Muscle group": "Forearms"}
forearm_supplemental_2 = {"Exercise": "Forearm bench support palms up",
                          "Instruction": "\nLeft first, count to 30 inbetween",
                          "Weight": "12.5kg", "Reps": "16", "Muscle group": "Forearms"}

# Forearms stretches
forearm_stretch_1 = {"Exercise": "Forearm stretch,\npull back middle two fingers", "Weight": "body", "Reps": "20"}

# Forearms warmup
forearm_warmup_1 = {"Exercise": "Forearm bench support palms down",
                    "Instruction": "\nLeft first, count to 30 inbetween",
                    "Weight": "5kg", "Reps": "14", "Muscle group": "Forearms"}
forearm_warmup_2 = {"Exercise": "Forearm bench support palms up",
                    "Instruction": "\nLeft first, count to 30 inbetween",
                    "Weight": "12.5kg", "Reps": "16", "Muscle group": "Forearms"}

# Biceps working exercises
biceps_work_1 = {"Exercise": "Bicep curl barbell", "Instruction":
                 "Squeeze at the top\nShoulder width grip\nReset each time",
                 "Weight": "22kg", "Reps": "8", "Muscle group": "Biceps"}
biceps_work_2 = {"Exercise": "Bicep curl reverse grip barbell", "Instruction": "thumbs up",
                 "Weight": "19.2kg", "Reps": "8", "Muscle group": "Biceps"}

# Biceps stretches
biceps_stretch_1 = {"Exercise": "Bicep curl barbell",
                    "Instruction": "Wide grip\n", "Weight": "0kg", "Reps": "20", "Muscle group": "Biceps"}
biceps_stretch_2 = {"Exercise": "Bicep curl reverse grip barbell", "Instruction": "thumbs up",
                    "Weight": "0kg", "Reps": "20", "Muscle group": "Biceps"}

# Biceps warmup
biceps_warmup_1 = {"Exercise": "EZ Bicep curl barbell", "Instruction":
                   "Squeeze at the top\nShoulder width grip\nReset each time",
                   "Weight": "14kg, 21kg, 29kg, 32kg", "Reps": "12, 10, 8, 8", "Muscle group": "Biceps"}
biceps_warmup_2 = {"Exercise": "Bicep curl reverse grip barbell", "Instruction": "thumbs up",
                   "Weight": "0kg, 4.6kg, 9.2kg, 15kg", "Reps": "12, 11, 10, 9", "Muscle group": "Biceps"}

# Triceps working exercises
triceps_work_1 = {"Exercise": "Straight bar standing triceps extension",
                  "Instruction": "Thumbs down\nDon't let elbows flare"
                  "\nMin width on textured area"
                  "\n(previous 4inch grip)\nTry to bend bar inward\nSqueeze triceps at top", "Weight": "16kg",
                  "Reps": "12", "Muscle group": "Triceps"}

# Triceps supplemental exercises
triceps_supplemental_1 = {"Exercise": "Bench Triceps kickback", "Instruction": "Lie on bench\nSqueeze triceps at top",
                          "Weight": "10kg", "Reps": "24", "Muscle group": "Triceps"}
triceps_supplemental_2 = {"Exercise": "Barbell skull crush",
                          "Instruction": "\nLand behind the head\nNatuarl grip above shoulders"
                          "\nDon't let elbows flare, should only be"
                          "visibile at top\nTry to bend bar inward\nSqueeze triceps at top",
                          "Weight": "8kg", "Reps": "25", "Muscle group": "Triceps"}
triceps_supplemental_3 = {"Exercise": "Straight bar standing triceps extension",
                          "Instruction": "humbs down\nDon't let elbows flare"
                          "\nMin width on textured area"
                          "\n(previous 4inch grip)\nTry to bend bar inward\nSqueeze triceps at top", "Weight": ""
                          "10kg, 15kg, 20kg, 22kg, 23kg", "Reps": "12-8", "Muscle group": "Triceps"}

# Triceps stretches
triceps_stretch_1 = {"Exercise": "Dynamic over head triceps stretching", "Weight": "BodyWeight", "Reps": "20"}

# Triceps warmup
triceps_warmup_1 = {"Exercise": "Straight bar standing triceps extension, thumbs down\nclosest grip",
                    "Weight": "0kg", "Reps": "20"}
triceps_warmup_2 = {"Exercise": "kickback Triceps extensions", "Weight": "5kg", "Reps": "25"}
triceps_warmup_3 = {"Exercise": "Overhead straight bar tricep extension", "Weight": "3kg", "Reps": "20"}

# Legs working exercises
legs_work_1 = {"Exercise": "Barbell squat past 90o",
               "Instruction": "Legs shoulder width apart, toes pointing only slightly out, "
               "trying to sit on a chair\nGo further if comfortable", "Weights": "76kg",
               "Reps": "3", "Muscle group": "Legs"}
legs_work_2 = {"Exercise": "Romainian deadlift", "Instruction": "\nGo long, stretch back with hams first, "
               "keep shins in place", "Weight": "60kg", "Reps": "5", "Muscle group": "Legs"}

# Legs supplemental exercises
legs_supplemental_1 = {"Exercise": "Machine dual calve raises",
                       "Instruction": "Full extension\nBoth Legs\nTwo seconds pause\nPush the round bar away from you",
                       "Weight": "30kg", "Reps": "10", "Muscle group": "Legs"}
legs_supplemental_2 = {"Exercise": "Romainian deadlift", "Instruction": "\nGo long, stretch back with hams first, "
                       "keep shins in place", "Weight": "32kg", "Reps": "8", "Muscle group": "Legs"}
legs_supplemental_3 = {"Exercise": "Machine dual tibia  raises",
                       "Instruction": "Full extension\nBoth Legs\nTwo seconds pause\nPush the round bar away from you",
                       "Weight": "17kg", "Reps": "20", "Muscle group": "Legs"}

# Legs stretches
legs_stretches_1 = {"Exercise": "Thigh stretch", "Weight": "BodyWeight", "Reps": "20"}
legs_stretches_2 = {"Exercise": "Hamstring stretch", "Weight": "BodyWeight", "Reps": "20"}
legs_stretches_3 = {"Exercise": "Plantar Fascia Stretches", "Weight": "BodyWeight", "Reps": "10"}

# Legs warmup
legs_warmup_1 = {"Exercise": "Barbell squat past 90o",
                 "Instruction": "Legs shoulder width apart, toes pointing only slightly out, "
                 "trying to sit on a chair\nGo further if comfortable", "Weights": "32kg, 47kg, 63kg, 70kg",
                 "Reps": "10, 8, 6, 3", "Muscle group": "Legs"}
legs_warmup_2 = {"Exercise": "Romainian deadlift", "Instruction": "\nGo long, stretch back with hams first, "
                 "keep shins in place", "Weight": "15kg, 21kg, 27kg, 35kg", "Reps": "12-9", "Muscle group": "Legs"}

# Back working exercises
back_work_1 = {"Exercise": "Deadlift Barbell\n", "Instruction": "\n1. Take stance one inch from bar, toes out a little "
               "\n2. Take grip just on the "
               "outside of my shins \n3. Knees forward to touch arms, knees to touch the inside of arms "
               "\n4. Don't drop ass too far, but below shoulders\n5. Pull the bar Squeese out chest, engage lats \n"
               "6. Push feet into the floor",
               "Weight": "81kg", "Reps": "3", "Muscle group": "Back"}
back_work_2 = {"Exercise": "Barbell row", "Instruction": "45o focus on mid back", "\nSet 1": "71kg", "Reps": "3",
               "Muscle group": "Back"}
back_work_3 = {"Exercise": "Pull up",
               "Instruction": "Overhand grip\nLead with chest\nFull ROM\nKeep glutes flexed",
               "\nWeight": "0kg", "Reps": "10", "Muscle group": "Back"}

# Back supplemental exercises
back_supplemental_1 = {"Exercise": "Dumbbell shrug\nNeutral both", "Instruction": "\n", "Weight": "25kg",
                       "Reps": "35", "Muscle group": "Back"}
back_supplemental_2 = {"Exercise": "Bench dumbbells row", "Instruction": "On knee with one arm", "Weight": "20kg",
                       "Reps": "16", "Muscle group": "Back"}
back_supplemental_3 = {"Exercise": "Barbell row", "Instruction": "45o focus on mid back",
                       "\nSet 1": "31kg, 45kg, 61kg, 68kg", "Reps": "6", "Muscle group": "Back"}
back_supplemental_4 = {"Exercise": "Pull up",
                       "Instruction": "Overhand grip\nLead with chest\nFull ROM\nKeep glutes flexed",
                       "\nSet 1": "0kg", "Reps": "3", "Muscle group": "Back"}
back_supplemental_5 = {"Exercise": "Barbell shrug\nLead forward from hips 10o", "Instruction": "\n", "Weight": "38kg",
                       "Reps": "18", "Muscle group": "Back"}

# Back stretches
back_stretch_1 = {"Exercise": "Touch toes", "Weight": "Bodyweight", "Reps": "20"}
back_stretch_2 = {"Exercise": "Dynamic cobra\nFocus on mid-back", "Weight": "BodyWeight", "Reps": "20"}

# Back warmup
back_warmup_1 = {"Exercise": "Barbell Shrug", "Weight": "5kg", "Reps": "30"}
back_warmup_2 = {"Exercise": "Barbell row", "Instruction": "45o focus on mid back",
                 "\nSet 1": "20kg, 40kg, 55kg, 65kg", "Reps": "10, 8, 6, 3",
                 "Muscle group": "Back"}
back_warmup_3 = {"Exercise": "Romainan Deadlift", "Weight": "0kg", "Reps": "20"}
back_warmup_4 = {"Exercise": "Deadlift Barbell\n",
                 "Instruction": "\n1. Take stance one inch from bar, toes out a little "
                 "\n2. Take grip just on the "
                 "outside of my shins \n3. Knees forward to touch arms, knees to touch the inside of arms "
                 "\n4. Don't drop ass too far, but below shoulders\n5. Pull the bar Squeese out chest, engage lats \n"
                 "6. Push feet into the floor",
                 "Weight": "35kg, 52kg, 70kg, 78kg", "Reps": "10, 8, 6, 3", "Muscle group": "Back"}
back_warmup_5 = {"Exercise": "Pull up",
                 "Instruction": "Overhand grip\nUse grip almost all the way out\n"
                                "Lead with chest\nFull ROM\nKeep glutes flexed",
                 "\nWeight": "13kg", "Reps": "3, 4, 5, 6, 7", "Muscle group": "Back"}

# Chest working exercises
chest_work_1 = {"Exercise": "Incline_Bench_Press_Narrow_Grip",
                "Instruction": "Pull shoulders down to arch back four holes,"
                "from inner smooth, firm feet, elbows under wrist, work on bending the bar in", "Weight":
                "55kg", "Reps": "3", "Muscle group": "Chest"}
chest_work_2 = {"Exercise": "Bench_press_neutral",
                "Instruction": "Pull shoulders down first to arch back two holes, let the arms down and "
                "loose as you push your back into the bench and move to have eyes under the bar",
                "Weight": "65kg", "Reps": "3", "Muscle group": "Chest"}

# Chest supplemental exercises
chest_supplemental_1 = {"Exercise": "Dumbbell Oblique Side Bend", "Instruction": "Keep shoulders locked in place\n",
                        "Weight": "25kg", "Reps": "20", "Muscle group": "Core"}
chest_supplemental_2 = {"Exercise": "Chest Fly", "Instruction": "\nPress together hard at the top, aim for full ROM",
                        "Weight": "15kg", "Reps": "12", "Muscle group": "Chest"}

# Chest stretches
chest_stretch_1 = {"Exercise": "Bar only bench press", "Weight": "5kg", "Reps": "20"}
chest_stretch_2 = {"Exercise": "Bar only incline bench press", "Weight": "5kg", "Reps": "20"}
chest_stretch_3 = {"Exercise": "Rotator cuff standing rotate", "Weight": "2.5kg", "Reps": "20"}

# Chest warmup
chest_warmup_1 = {"Exercise": "Incline_Bench_Press_Narrow_Grip",
                  "Instruction": "Pull shoulders down to arch back four holes,"
                  "from inner smooth, firm feet, elbows under wrist, work on bending the bar in", "Weight":
                  "25kg, 38kg, 51kg, 57kg", "Reps": "10, 8, 6, 3", "Muscle group": "Chest"}
chest_warmup_2 = {"Exercise": "Bench_press_neutral",
                  "Instruction": "Pull shoulders down first to arch back two holes, let the arms down and "
                  "loose as you push your back into the bench and move to have eyes under the bar",
                  "Weight": "30kg, 45kg, 60kg, 66kg", "Reps": "10, 8, 6, 3", "Muscle group": "Chest"}

# delta = datetime.datetime(2021, 9, 9) - datetime.datetime.now()
now = datetime.datetime.now()
cw = (now.strftime("%A"))
start_time = time.time()
os.system("clear")
#cw = "Friday"


def get_current_stage(start_date, today):
    strength_duration = 12  # weeks
    transition_duration = 2  # weeks
    hypertrophy_duration = 12  # weeks
    deload_duration = 1  # week
    cycle_duration = strength_duration + transition_duration + hypertrophy_duration + transition_duration + deload_duration

    # Calculate the number of weeks since the start date
    weeks_elapsed = (today - start_date).days // 7

    # Calculate the current week within the training cycle
    current_cycle_week = weeks_elapsed % cycle_duration

    if 0 <= current_cycle_week < strength_duration:
        return "strength"
    elif strength_duration <= current_cycle_week < strength_duration + transition_duration:
        return "transition"
    elif strength_duration + transition_duration <= current_cycle_week < strength_duration + transition_duration + hypertrophy_duration:
        return "hypertrophy"
    else:
        return "transition"


# Example usage:
start_date = date(2023, 1, 1)
today = date.today()

current_stage = get_current_stage(start_date, today)
print("Current stage:", current_stage)


def countdown(t):
    while t:
        mins, secs = divmod(t, 60)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        print(timeformat, end='\r')
        time.sleep(1)
        t -= 1
    print('Countdown complete')


# Script variables
title = os.getlogin()
pwd = os.getcwd()
update = ('cd /Users/matthewferguson/Developer/weights_and_yoga/;'
          ' git add -A;git commit -m "finished a workout and made some changes";git push')
rep_count = "5"
dumbbell = ''
barbell = ''

Thursday_stretches = [triceps_stretch_1, shoulders_stretch_1, back_stretch_2, chest_stretch_1, chest_stretch_2]
Thursday_warmup = [chest_warmup_2, shoulders_supplemental_3, chest_warmup_1, biceps_warmup_1]
Thursday_exercises = [chest_work_2, shoulders_supplemental_3, chest_work_1, biceps_work_1]

Friday_stretches = [legs_stretches_1, legs_stretches_2, legs_stretches_3, back_stretch_1, back_stretch_2]
Friday_warmup = [legs_warmup_1, back_warmup_4, back_warmup_5, shoulders_warmup_1, back_warmup_2]
Friday_exercises = [legs_work_1, back_work_1, back_work_3, shoulders_work_1, back_work_2]

Sunday_stretches = [triceps_stretch_1, shoulders_stretch_1, back_stretch_2, chest_stretch_1, chest_stretch_2]
Sunday_warmup = [chest_warmup_2, shoulders_supplemental_3, chest_warmup_1, triceps_supplemental_3]
Sunday_exercises = [chest_work_2, shoulders_supplemental_3, chest_work_1, triceps_supplemental_3]

Tuesday_stretches = [biceps_stretch_1, biceps_stretch_2]
Tuesday_warmup = [biceps_warmup_1, biceps_warmup_2]
Tuesday_exercises = [biceps_work_1, biceps_work_2]


class Workout(object):
    def __init__(self, wo_n, b_Weight, d_Weight, reps, stretch, warmup, exercises):
        self.wo_n = wo_n
        self.b_Weight = b_Weight
        self.d_Weight = d_Weight
        self.reps = reps
        self.stretch = stretch
        self.warmup = warmup
        self.exercises = exercises

    def current_stretch(self):
            subprocess.call(["clear"])
            print("\n")
            for stretch in self.stretch:
                for item in stretch.items():
                    print(": ".join(item))
                print("\n")

    def current_warmup(self):
            subprocess.call(["clear"])
            ec = 1
            row = 3
            while ec < 5:
                for warmup in self.warmup:
                    for item in warmup.items():
                        print(": ".join(item))
                        if item[0] == "Reps":
                            projected_reps = item[1]
                    print("\nSet number: " + str(ec) + "\n")
                    elapsed_time = time.time() - start_time
                    print("You have been working out for: " +
                          time.strftime('%H:%M:%S', time.gmtime(int("%0.0f" % elapsed_time))))
                    print(self.wo_n)
                    complete_reps = input("Done?")
                    subprocess.call(["clear"])
                    if complete_reps == "":
                        complete_reps = projected_reps
                    row += 1
                print("Progress quickly through warmups")
                ec += 1
                subprocess.call(["clear"])

    def current_workout(self):
            subprocess.call(["clear"])
            ec = 1
            row = 3
            while ec < 3:
                for exercise in self.exercises:
                    for item in exercise.items():
                        print(": ".join(item))
                        if item[0] == "Reps":
                            projected_reps = item[1]
                    print("\nSet number: " + str(ec) + "\n")
                    elapsed_time = time.time() - start_time
                    print("You have been working out for: " +
                          time.strftime('%H:%M:%S', time.gmtime(int("%0.0f" % elapsed_time))))
                    print(self.wo_n)
                    countdown(60)
                    complete_reps = input("Please press any key to submit expected reps, or completed reps"
                                          ", when you have completed the exercise.")
                    subprocess.call(["clear"])
                    if complete_reps == "":
                        complete_reps = projected_reps
                    row += 1
                print("Rest & drink water in between sets")
                # countdown(180)
                ec += 1
                subprocess.call(["clear"])
            print("Congratulations " + title + ", you finished.")
            elapsed_time = time.time() - start_time
            print("It took you: " + time.strftime('%H:%M:%S', time.gmtime(int("%0.0f" % elapsed_time))))


Thursday = Workout(wo_n="Thursday", b_Weight="0", d_Weight=dumbbell, reps=rep_count, stretch=Thursday_stretches,
                   warmup=Thursday_warmup, exercises=Thursday_exercises)

Friday = Workout(wo_n="Friday", b_Weight="0", d_Weight=dumbbell, reps=rep_count, stretch=Friday_stretches,
                 warmup=Friday_warmup, exercises=Friday_exercises)

Sunday = Workout(wo_n="Sunday", b_Weight="0", d_Weight=dumbbell, reps=rep_count, stretch=Sunday_stretches,
                 warmup=Sunday_warmup, exercises=Sunday_exercises)

Tuesday = Workout(wo_n="Tuesday", b_Weight="0", d_Weight=dumbbell, reps=rep_count, stretch=Tuesday_stretches,
                  warmup=Tuesday_warmup, exercises=Tuesday_exercises)

# if current_stage is "strength":
workouts = [Tuesday, Thursday, Friday, Sunday]
subprocess.call(["clear"])
for workout in workouts:
    if cw == workout.wo_n:
        print("Today is: " + cw)
        workout.current_stretch()
        input("\nStretches complete?")
        print("*** WARMUP SETS ***")
        workout.current_warmup()
        subprocess.call(["clear"])
        countdown(330)
        print("*** WORKING SETS ***")
        workout.current_workout()
        os.system(update)
