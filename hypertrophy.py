#! /usr/bin/python3

import subprocess
import time
import datetime
import os
from datetime import date

__author__ = 'mferguson'

# Exercises
# https://www.verywellfit.com/the-plank-exercise-3120068
# https://www.verywellfit.com/how-to-do-a-hollow-body-hold-techniques-benefits-variations-5079591
# https://www.healthline.com/health/exercise-fitness/dead-bug-exercise#variations
# Shoulders working exercises
shoulders_work_1 = {"Exercise": "Behind Neck Barbell Press",
                    "Instruction": "Arms in tight, life-line grip to start", "Weight": "38kg",
                    "Reps": "8", "Muscle group": "Shoulders"}

# Shoulders supplemental exercises
shoulders_supplemental_1 = {"Exercise": "Dual frontal raise", "Instruction": "hold like hammers\n", "Weight": "10kg",
                            "Reps": "15", "Muscle group": "Shoulders"}
shoulders_supplemental_2 = {"Exercise": "One handed dumbbell bent over curl", "Instruction": "\n", "Weight": "15kg",
                            "Reps": "8"}
shoulders_supplemental_3 = {"Exercise": "Bent-Over reverse fly",
                            "Instruction": "Retract shoulders, straight arms\n", "Weight": "7.5kg, 10kg, 12.5kg, 15kg",
                            "\nReps": "22, 18, 16, 16, 16",
                            "Muscle group": "Shoulders"}
shoulders_supplemental_4 = {"Exercise": "Lateral raise",
                            "Instruction": "\n1. Neutral grip, with thumbs towards weight\n2. Shoulders retracted\n"
                            "3. Keep hands flat, don't raise pinky\n4. Focus on technique\n"
                            "5. Tense the glutes to prevent excessive movement\n"
                            "6. During lift stay in 15o to 30o rotation forward\n"
                            "7. Focus on time under tension, top of ROM to 45o\n",
                            "Weight": "5kg, 5kg, 7.5kg, 10kg, 12.5kg",
                            "\nReps": "16", "Muscle group": "Shoulders"}
shoulders_supplemental_5 = {"Exercise": "Seated Rear Delt\nLean over\nGrab weights by the back\nSmall movements"
                                        "\nfocus on pushing from thumbs\nKeep arms straight to protect triceps",
                            "Instruction": "Retract shoulders, straight arms\n",
                            "Weight": "10kg, 12.5kg, 15kg, 17.5kg",
                            "\nReps": "16", "Muscle group": "Shoulders"}

# Shoulders stretches
shoulders_stretch_1 = {"Exercise": "Resistance band pull apart", "Weight": "BodyWeight", "Reps": "20"}
shoulders_stretch_2 = {"Exercise": "Resistance band external rotator cuff extension", "Weight": "BodyWeight",
                       "Reps": "25"}
shoulders_stretch_3 = {"Exercise": "Resistance band internal rotator cuff extension", "Weight": "BodyWeight",
                       "Reps": "25"}
shoulders_stretch_4 = {"Exercise": "Empty Bar", "Weight": "0kg",
                       "Reps": "12"}

# Shoulders warmup
shoulders_warmup_1 = {"Exercise": "Behind Neck Barbell Press",
                      "Instruction": "Stop behind the mid head, to protect scapula\n"
                                     "Arms in tight, life-line grip to start", "Weight": "20kg, 21kg, 22kg, 23kg, 25kg",
                      "Reps": "12", "Muscle group": "Shoulders"}
shoulders_warmup_2 = {"Exercise": "Bent-Over reverse fly",
                      "Instruction": "Retract shoulders, straight arms\n", "Weight": "7.5kg, 10kg, 12.5kg",
                      "\nReps": "12-8",
                      "Muscle group": "Shoulders"}

# Forearms working exercises
forearm_work_1 = {"Exercise": "Reverse grip Ez curl",
                  "Instruction": "Don't use thumbs for grip\nDo top half of exercise\nThumbs up",
                  "Weight": "6kg", "Reps": "9", "Muscle group": "Forearms"}
forearm_work_2 = {"Exercise": "Reverse barbell curl",
                  "Instruction": "Don't use thumbs for grip\nDo top half of exercise\nThumbs up",
                  "Weight": "5kg, 7kg, 8kg, 10kg, 11kg", "Reps": "12", "Muscle group": "Forearms"}
forearm_work_3 = {"Exercise": "Suitcase lift", "Instruction": "\n", "Weight": "30kg, 35kg, 40kg, 45kg, 50kg",
                  "Reps": "35 seconds", "Muscle group": "Forearms"}

# Forearms supplemental exercises
forearm_supplemental_1 = {"Exercise": "Forearm bench support palms down",
                          "Instruction": "\nLeft first, count to 30 in between",
                          "Weight": "5kg", "Reps": "14", "Muscle group": "Forearms"}
forearm_supplemental_2 = {"Exercise": "Forearm bench support palms up",
                          "Instruction": "\nLeft first, count to 30 in between",
                          "Weight": "12.5kg", "Reps": "16", "Muscle group": "Forearms"}

# Forearms stretches
forearm_stretch_1 = {"Exercise": "Forearm stretch,\npull back middle two fingers", "Weight": "body", "Reps": "20"}

# Forearms warmup
forearm_warmup_1 = {"Exercise": "Forearm bench support palms down",
                    "Instruction": "\nLeft first, count to 30 in between",
                    "Weight": "5kg", "Reps": "14", "Muscle group": "Forearms"}
forearm_warmup_2 = {"Exercise": "Forearm bench support palms up",
                    "Instruction": "\nLeft first, count to 30 in between",
                    "Weight": "12.5kg", "Reps": "16", "Muscle group": "Forearms"}

# Biceps working exercises
biceps_work_1 = {"Exercise": "EZ Bicep curl barbell", "Instruction":
                 "Squeeze at the top\nShoulder width grip\nReset each time",
                 "Weight": "32kg", "Reps": "8", "Muscle group": "Biceps"}
biceps_work_2 = {"Exercise": "Bicep curl reverse grip barbell", "Instruction": "thumbs up",
                 "Weight": "32kg", "Reps": "8", "Muscle group": "Biceps"}


# Biceps stretches
biceps_stretch_1 = {"Exercise": "Hammer Bicep curl dumbbells",
                    "Instruction": "Standard grip\n", "Weight": "5kg", "Reps": "12", "Muscle group": "Biceps"}
biceps_stretch_2 = {"Exercise": "Bicep curl dumbbells", "Instruction": "Hammer grip",
                    "Weight": "5kg", "Reps": "12", "Muscle group": "Biceps"}

# Biceps warmup
biceps_warmup_1 = {"Exercise": "Hammer Bicep curl dumbbell", "Instruction":
                   "Squeeze at the top\nShoulder width grip\nReset each time",
                   "Weight": "7.5kg, 10kg, 12.5kg, 15kg", "Reps": "12", "Muscle group": "Biceps"}
biceps_warmup_2 = {"Exercise": "Bicep curl dumbbell", "Instruction": "thumbs up",
                   "Weight": "5kg, 7.5kg, 10kg, 12.5kg, 12.5kg", "Reps": "8", "Muscle group": "Biceps"}

# Triceps working exercises
triceps_work_1 = {"Exercise": "Straight bar standing triceps extension",
                  "Instruction": "Thumbs down\nDon't let elbows flare"
                  "\nMin width on textured area"
                  "\n(previous 4inch grip)\nTry to bend bar inward\nSqueeze triceps at top", "Weight": "16kg",
                  "Reps": "12", "Muscle group": "Triceps"}

# Triceps supplemental exercises
triceps_supplemental_1 = {"Exercise": "Bench Triceps kickback", "Instruction": "Lie on bench\nSqueeze triceps at top",
                          "Weight": "10kg", "Reps": "24", "Muscle group": "Triceps"}
triceps_supplemental_2 = {"Exercise": "Barbell skull crush",
                          "Instruction": "\nLand behind the head\nNatural grip above shoulders"
                          "\nDon't let elbows flare, should only be"
                          "visibile at top\nTry to bend bar inward\nSqueeze triceps at top",
                          "Weight": "8kg", "Reps": "25", "Muscle group": "Triceps"}
triceps_supplemental_3 = {"Exercise": "Straight bar standing triceps extension",
                          "Instruction": "humbs down\nDon't let elbows flare"
                          "\nMin width on textured area"
                          "\n(previous 4inch grip)\nTry to bend bar inward\nSqueeze triceps at top", "Weight": ""
                          "11kg, 17kg, 22kg, 25kg, 28kg", "Reps": "12-8", "Muscle group": "Triceps"}

# Triceps stretches
triceps_stretch_1 = {"Exercise": "Dynamic over head triceps stretching", "Weight": "BodyWeight", "Reps": "20"}

# Triceps warmup
triceps_warmup_1 = {"Exercise": "Straight bar standing triceps extension, thumbs down\nclosest grip",
                    "Weight": "0kg", "Reps": "20"}
triceps_warmup_2 = {"Exercise": "kickback Triceps extensions", "Weight": "5kg", "Reps": "25"}
triceps_warmup_3 = {"Exercise": "Overhead straight bar tricep extension", "Weight": "3kg", "Reps": "20"}

# Legs working exercises
legs_work_1 = {"Exercise": "High barbell squat past 90o",
               "Instruction": "Legs shoulder width apart, toes pointing only slightly out, "
               "trying to sit on a chair\nGo further if comfortable", "Weights": "50kg",
               "Reps": "12", "Muscle group": "Legs"}
legs_work_2 = {"Exercise": "Romainian Deadlift", "Instruction": "\nGo long, stretch back with hams first, "
               "keep shins in place", "Weight": "58kg", "Reps": "8", "Muscle group": "Legs"}

# Legs supplemental exercises
legs_supplemental_1 = {"Exercise": "Machine dual calve raises",
                       "Instruction": "Full extension\nBoth Legs\nTwo seconds pause\nPush the round bar away from you",
                       "Weight": "25kg, 30kg, 35kg, 41kg, 48kg", "Reps": "20-16", "Muscle group": "Legs"}
legs_supplemental_2 = {"Exercise": "Romainian Deadlift", "Instruction": "\nGo long, stretch back with hams first, "
                       "keep shins in place", "Weight": "32kg", "Reps": "8", "Muscle group": "Legs"}
legs_supplemental_3 = {"Exercise": "Machine dual tibia  raises",
                       "Instruction": "Full extension\nBoth Legs\nTwo seconds pause\nPush the round bar away from you",
                       "Weight": "17kg", "Reps": "20", "Muscle group": "Legs"}

# Legs stretches
legs_stretches_1 = {"Exercise": "Thigh stretch", "Weight": "BodyWeight", "Reps": "20"}
legs_stretches_2 = {"Exercise": "Hamstring stretch", "Weight": "BodyWeight", "Reps": "20"}
legs_stretches_3 = {"Exercise": "Plantar Fascia Stretches", "Weight": "BodyWeight", "Reps": "10"}

# Legs warmup
legs_warmup_1 = {"Exercise": "High barbell squat past 90o",
                 "Instruction": "Legs shoulder width apart, toes pointing only slightly out, "
                 "trying to sit on a chair\nGo further if comfortable", "Weights": "20kg, 35kg, 40kg, 45kg",
                 "Reps": "12", "Muscle group": "Legs"}
legs_warmup_2 = {"Exercise": "Romainian deadlift", "Instruction": "\nGo long, stretch back with hams first, "
                 "keep shins in place", "Weight": "25kg, 38kg, 50kg, 56kg", "Reps": "12-9", "Muscle group": "Legs"}
legs_warmup_3 = {"Exercise": "Good morning", "Instruction": "\nGo long, stretch back with hams first,"
                                                "low bar resting on delts, small feet flare, just over shoulder width"
                 "keep shins in place", "Weight": "20kg, 22kg, 25kg, 28kg", "Reps": "12", "Muscle group": "Legs"}

# Back working exercises
back_work_1 = {"Exercise": "Deadlift Barbell\n", "Instruction": "\n1. Take stance one inch from bar, toes out a little "
               "\n2. Take grip just on the "
               "outside of my shins \n3. Knees forward to touch arms, knees to touch the inside of arms "
               "\n4. Don't drop ass too far, but below shoulders\n5. Pull the bar Squeeze out chest, engage lats \n"
               "6. Push feet into the floor",
               "Weight": "65kg", "Reps": "12", "Muscle group": "Back"}
back_work_2 = {"Exercise": "Barbell row", "Instruction": "45o focus on mid back", "\nSet 1": "72kg", "Reps": "14",
               "Muscle group": "Back"}

# Back supplemental exercises
back_supplemental_1 = {"Exercise": "Dumbbell shrug\nNeutral both", "Instruction": "\n", "Weight": "25kg",
                       "Reps": "35", "Muscle group": "Back"}
back_supplemental_2 = {"Exercise": "Bench dumbbells row", "Instruction": "On knee with one arm", "Weight": "20kg",
                       "Reps": "16", "Muscle group": "Back"}
back_supplemental_3 = {"Exercise": "Barbell row", "Instruction": "45o focus on mid back",
                       "\nSet 1": "20kg, 30kg, 36kg, 40kg", "Reps": "6", "Muscle group": "Back"}
back_supplemental_4 = {"Exercise": "Pull up",
                       "Instruction": "Overhand grip\nLead with chest\nFull ROM\nKeep glutes flexed",
                       "\nReps": "4, 5, 6, 7, 8", "Weight": "0kg", "Muscle group": "Back"}
back_supplemental_5 = {"Exercise": "Barbell shrug\nLead forward from hips 10o", "Instruction": "\n", "Weight": "38kg",
                       "Reps": "18", "Muscle group": "Back"}

#Back Stretches
back_stretch_1 = {"Exercise": "Touch toes", "Weight": "Bodyweight", "Reps": "20"}
back_stretch_2 = {"Exercise": "Dynamic cobra\nFocus on mid-back", "Weight": "BodyWeight", "Reps": "20"}

# Back warmup
back_warmup_1 = {"Exercise": "Barbell Shrug", "Weight": "5kg", "Reps": "30"}
back_warmup_2 = {"Exercise": "Barbell row", "Instruction": "45o focus on mid back",
                 "\nSet 1": "30kg, 40kg, 50kg, 60kg, 65kg", "Reps": "16",
                 "Muscle group": "Back"}
back_warmup_3 = {"Exercise": "Romainan Deadlift", "Weight": "0kg", "Reps": "20"}
back_warmup_4 = {"Exercise": "Deadlift Barbell\n",
                 "Instruction": "\n1. Take stance one inch from bar, toes out a little "
                 "\n2. Take grip just on the "
                 "outside of my shins \n3. Knees forward to touch arms, knees to touch the inside of arms "
                 "\n4. Don't drop ass too far, but below shoulders\n5. Pull the bar Squeese out chest, engage lats \n"
                 "6. Push feet into the floor",
                 "Weight": "30kg, 40kg, 50kg, 60kg", "Reps": "12", "Muscle group": "Back"}
back_warmup_5 = {"Exercise": "Pull up",
                 "Instruction": "Overhand grip\nUse grip almost all the way out\n"
                                "Lead with chest\nFull ROM\nKeep glutes flexed",
                 "\nWeight": "0kg", "Reps": "5", "Muscle group": "Back"}

# Chest working exercises
chest_work_1 = {"Exercise": "Narrow_Grip_Incline_Bench_Press",
                "Instruction": "Pull shoulders down to arch back,"
                "\nSet bar for four holes,"
                "\nGrip bar as close as the bar grip allows,"
                "from inner smooth, firm feet, elbows under wrist, work on bending the bar in", "Weight":
                "53kg", "Reps": "8", "Muscle group": "Chest"}
chest_work_2 = {"Exercise": "Narrow_Grip_Bench_press_neutral",
                "Instruction": "Pull shoulders down first to arch back two holes,"
                "\nGrip bar as close as the bar grip allows\n"
                "Set bar to two holes\nlet the arms down and "
                "loose as you push your back into the bench and move to have eyes under the bar",
                "Weight": "60kg", "Reps": "8", "Muscle group": "Chest"}

# Chest supplemental exercises
chest_supplemental_1 = {"Exercise": "Dumbbell Oblique Side Bend", "Instruction": "Keep shoulders locked in place\n"
                                                                                 "Put hand on side of head\n"
                                                                                 "Focus on touching a button with elbow"
                                                                                 "\n",
                        "Weight": "5kg, 10kg, 15kg, 20kg, 25kg", "Reps": "16", "Muscle group": "Core"}
chest_supplemental_2 = {"Exercise": "Chest Fly", "Instruction": "\nPress together hard at the top, aim for full ROM",
                        "Weight": "15kg", "Reps": "12", "Muscle group": "Chest"}

# Chest stretches
chest_stretch_1 = {"Exercise": "Bar only bench press", "Weight": "0kg", "Reps": "16"}
chest_stretch_2 = {"Exercise": "Bar only incline bench press", "Weight": "5kg", "Reps": "20"}
chest_stretch_3 = {"Exercise": "Rotator cuff standing rotate", "Weight": "2.5kg", "Reps": "20"}

# Chest warmup
chest_warmup_1 = {"Exercise": "Narrow_Grip_Incline_Bench_Press_Narrow_Grip",
                  "Instruction": "Pull shoulders down to arch back,"
                  "\nSet bar for four holes,"
                  "\nGrip bar as close as the bar grip allows,"
                  "from inner smooth, firm feet, elbows under wrist, work on bending the bar in", "Weight":
                  "20kg, 25kg, 30kg, 35kg", "Reps": "12", "Muscle group": "Chest"}
chest_warmup_2 = {"Exercise": "Narrow_Grip_Bench_press_neutral",
                  "Instruction": "Pull shoulders down to arch back,"
                  "\nSet bar for four holes,"
                  "\nGrip bar as close as the bar grip allows,"
                  "from inner smooth, firm feet, elbows under wrist, work on bending the bar in",
                  "Weight": "25kg, 30kg, 35kg, 40kg", "Reps": "12", "Muscle group": "Chest"}

# Neck Stretches
Neck_stretch_1 = {"Exercise": "Neck Stretch", "Weight": "1kg, 2kg, 3kg, 5kg, 6kg", "Reps": "15"}

# Neck Warmup
Neck_warmup_1 = {"Exercise": "Neck rise", "Weight": "2.5kg, 3.5kg, 5kg, 6kg, 7kg", "Reps": "15"}

# Core exercises
core_exercise_1 = {"Exercise": "Weighted Sit-ups", "Weight": "2.5kg, 5kg", "Reps": "12"}
core_exercise_2 = {"Exercise": "Lying Leg Raises", "Weight": "BodyWeight", "Reps": "12"}


# delta = datetime.datetime(2021, 9, 9) - datetime.datetime.now()
now = datetime.datetime.now()
cw = (now.strftime("%A"))
start_time = time.time()
os.system("clear")
#cw = "Monday"


def get_current_stage(start_date, today):
    strength_duration = 12  # weeks
    transition_duration = 2  # weeks
    hypertrophy_duration = 12  # weeks
    deload_duration = 1  # week
    cycle_duration = strength_duration + transition_duration + hypertrophy_duration + transition_duration + deload_duration

    # Calculate the number of weeks since the start date
    weeks_elapsed = (today - start_date).days // 7

    # Calculate the current week within the training cycle
    current_cycle_week = weeks_elapsed % cycle_duration

    if 0 <= current_cycle_week < strength_duration:
        return "strength"
    elif strength_duration <= current_cycle_week < strength_duration + transition_duration:
        return "transition"
    elif strength_duration + transition_duration <= current_cycle_week < strength_duration + transition_duration + hypertrophy_duration:
        return "hypertrophy"
    else:
        return "transition"


# Example usage:
start_date = date(2023, 1, 1)
today = date.today()

current_stage = get_current_stage(start_date, today)
print("Current stage:", current_stage)


def countdown(t):
    while t:
        mins, secs = divmod(t, 60)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        print(timeformat, end='\r')
        time.sleep(1)
        t -= 1
    print('Countdown complete')


# Script variables
title = os.getlogin()
pwd = os.getcwd()
update = ('cd /Users/matthewferguson/Developer/weights_and_yoga/;'
          ' git add -A;git commit -m "finished a workout and made some changes";git push')
rep_count = "5"
dumbbell = ''
barbell = ''

"""
Day 1 (Thursday) - Lower Body Focus
1. behind the neck Overhead Press
2. Squat
3. Romanian Deadlift
4. Rear delt fly
5. Calve Raises

Day 2 (Friday) - Upper Body Focus
1. Bench Press
2. Barbell Row
3. Incline Bench Press
4. Tricep extensions

Day 3 (Sunday) - Full Body
1. Deadlift
2. Behind the neck Overhead Press
3. Pull-Ups or Chin-Ups
4. Barbell Curl
"""
# Good Morning and behind the neck
Monday_stretches = [legs_stretches_2, legs_stretches_3, shoulders_stretch_1, back_stretch_1, back_stretch_2,
                    shoulders_stretch_4]
Monday_warmup = [legs_warmup_3, shoulders_warmup_1]
Monday_exercises = [legs_warmup_3, shoulders_warmup_1]

# Bench press and Squat
Tuesday_stretches = [legs_stretches_1, legs_stretches_2, legs_stretches_3, shoulders_stretch_1, back_stretch_1,
                     back_stretch_2, chest_stretch_1]
Tuesday_warmup = [chest_warmup_2, legs_warmup_1]
Tuesday_exercises = [chest_warmup_2, legs_warmup_1]

# Biceps and pull up
Wednesday_stretches = [legs_stretches_1, legs_stretches_2, legs_stretches_3, shoulders_stretch_1,
                       back_stretch_2, biceps_warmup_1]
Wednesday_warmup = [biceps_warmup_1, back_warmup_5]
Wednesday_exercises = [biceps_warmup_1, back_warmup_5]

# Deadlifts & Triceps
Thursday_stretches = [legs_stretches_1, legs_stretches_2, legs_stretches_3, shoulders_stretch_1, back_stretch_1,
                     back_stretch_2]
Thursday_warmup = [back_warmup_4, back_warmup_5]
Thursday_exercises = [back_warmup_4, triceps_work_1]

# Barbell row and rear delts
Friday_stretches = [shoulders_stretch_1, back_stretch_1, back_stretch_2, shoulders_stretch_4]
Friday_warmup = [back_supplemental_3, shoulders_supplemental_5]
Friday_exercises = [back_supplemental_3, shoulders_supplemental_5]

# Obliques and neck
Saturday_stretches = [shoulders_stretch_1, back_stretch_1, back_stretch_2, shoulders_stretch_4]
Saturday_warmup = [chest_supplemental_1, Neck_warmup_1]
Saturday_exercises = [chest_supplemental_1, Neck_warmup_1]

# Core
Sunday_stretches = [shoulders_stretch_1, back_stretch_1, back_stretch_2, shoulders_stretch_4]
Sunday_warmup = [core_exercise_1, core_exercise_2]
Sunday_exercises = [core_exercise_1, core_exercise_2]



class Workout(object):
    def __init__(self, wo_n, b_Weight, d_Weight, reps, stretch, warmup, exercises):
        self.wo_n = wo_n
        self.b_Weight = b_Weight
        self.d_Weight = d_Weight
        self.reps = reps
        self.stretch = stretch
        self.warmup = warmup
        self.exercises = exercises

    def current_stretch(self):
            subprocess.call(["clear"])
            print("\n")
            for stretch in self.stretch:
                for item in stretch.items():
                    print(": ".join(item))
                print("\n")

    def current_warmup(self):
            subprocess.call(["clear"])
            ec = 1
            row = 3
            while ec < 5:
                for warmup in self.warmup:
                    for item in warmup.items():
                        print(": ".join(item))
                        if item[0] == "Reps":
                            projected_reps = item[1]
                    print("\nSet number: " + str(ec) + "\n")
                    elapsed_time = time.time() - start_time
                    print("You have been working out for: " +
                          time.strftime('%H:%M:%S', time.gmtime(int("%0.0f" % elapsed_time))))
                    print(self.wo_n)
                    complete_reps = input("Done?")
                    subprocess.call(["clear"])
                    if complete_reps == "":
                        complete_reps = projected_reps
                    row += 1
                print("Progress quickly through warmups")
                ec += 1
                subprocess.call(["clear"])

    def current_workout(self):
            subprocess.call(["clear"])
            ec = 1
            row = 3
            while ec < 3:
                for exercise in self.exercises:
                    for item in exercise.items():
                        print(": ".join(item))
                        if item[0] == "Reps":
                            projected_reps = item[1]
                    print("\nSet number: " + str(ec) + "\n")
                    elapsed_time = time.time() - start_time
                    print("You have been working out for: " +
                          time.strftime('%H:%M:%S', time.gmtime(int("%0.0f" % elapsed_time))))
                    print(self.wo_n)
                    countdown(60)
                    complete_reps = input("Please press any key to submit expected reps, or completed reps"
                                          ", when you have completed the exercise.")
                    subprocess.call(["clear"])
                    if complete_reps == "":
                        complete_reps = projected_reps
                    row += 1
                print("Rest & drink water in between sets")
                # countdown(180)
                ec += 1
                subprocess.call(["clear"])
            print("Congratulations " + title + ", you finished.")
            elapsed_time = time.time() - start_time
            print("It took you: " + time.strftime('%H:%M:%S', time.gmtime(int("%0.0f" % elapsed_time))))


Monday = Workout(wo_n="Monday", b_Weight="0", d_Weight=dumbbell, reps=rep_count, stretch=Monday_stretches,
                   warmup=Monday_warmup, exercises=Monday_exercises)

Wednesday = Workout(wo_n="Wednesday", b_Weight="0", d_Weight=dumbbell, reps=rep_count, stretch=Wednesday_stretches,
                 warmup=Wednesday_warmup, exercises=Wednesday_exercises)

Friday = Workout(wo_n="Friday", b_Weight="0", d_Weight=dumbbell, reps=rep_count, stretch=Friday_stretches,
                 warmup=Friday_warmup, exercises=Friday_exercises)


# if current_stage is "strength":
workouts = [Monday, Wednesday, Friday]
subprocess.call(["clear"])
for workout in workouts:
    if cw == workout.wo_n:
        print("Today is: " + cw)
        workout.current_stretch()
        input("\nStretches complete?")
        print("*** WARMUP SETS ***")
        workout.current_warmup()
        subprocess.call(["clear"])
        countdown(330)
        print("*** WORKING SETS ***")
        workout.current_workout()
        os.system(update)
