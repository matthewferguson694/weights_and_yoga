#!/usr/bin/env python3

def main():
    print("Please enter the following measurements in centimeters. Measure at the largest point of each body part:")

    wrist_circumference = float(input("Wrist circumference (cm) (measure just above the knobby bone): "))
    neck_circumference = float(input("Neck circumference (cm) (measure just below the larynx with tape slightly "
                                     "angled downward to the front): "))
    left_upper_arm_circumference = float(input("Left upper arm circumference (cm) (measure relaxed arm at the largest "
                                               "point, which is typically at the peak of the biceps): "))
    right_upper_arm_circumference = float(input("Right upper arm circumference (cm) (measure relaxed arm at the "
                                                "largest point, which is typically at the peak of the biceps): "))
    chest_circumference = float(input("Chest circumference (cm) (measure around the largest part of the chest, "
                                      "usually at the nipple line): "))
    waist_circumference = float(input("Waist circumference (cm) (measure at the narrowest part of the waist, "
                                      "usually just above the belly button): "))
    hips_circumference = float(input("Hips circumference (cm) (measure around the largest part of the hips): "))
    left_thigh_circumference = float(input("Left thigh circumference (cm) (measure the largest part of the thigh, "
                                           "just below the buttock): "))
    right_thigh_circumference = float(input("Right thigh circumference (cm) (measure the largest part of the thigh, "
                                            "just below the buttock): "))
    left_calf_circumference = float(input("Left calf circumference (cm) (measure at the largest part of the calf): "))
    right_calf_circumference = float(input("Right calf circumference (cm) (measure at the largest part of the calf): "))
    weight = float(input("Weight in kilograms: "))

    ideal_neck_ratio = wrist_circumference
    ideal_upper_arm_ratio = wrist_circumference
    ideal_chest_ratio = wrist_circumference * 6.5
    ideal_waist_ratio = wrist_circumference * 4.25
    ideal_hips_ratio = wrist_circumference * 5.5
    ideal_thigh_ratio = wrist_circumference * 2.34
    ideal_calf_ratio = wrist_circumference * 1.875
    ideal_weight = (180 - 100) * 0.9

    print("\nDesired measurements based on the Grecian ideal proportions:")
    print(f"Wrist: {wrist_circumference:.2f} cm (unchangeable)")
    print(f"Neck: {ideal_neck_ratio:.2f} cm")
    print(f"Upper arms: {ideal_upper_arm_ratio:.2f} cm")
    print(f"Chest: {ideal_chest_ratio:.2f} cm")
    print(f"Waist: {ideal_waist_ratio:.2f} cm")
    print(f"Hips: {ideal_hips_ratio:.2f} cm")
    print(f"Thighs: {ideal_thigh_ratio:.2f} cm")
    print(f"Calves: {ideal_calf_ratio:.2f} cm")
    print(f"Weight: {ideal_weight:.2f} kg")

    print("\nAreas that need work to get closer to the ratio:")
    if neck_circumference < ideal_neck_ratio:
        print(f"Increase neck circumference to {ideal_neck_ratio:.2f} cm")
    if left_upper_arm_circumference < ideal_upper_arm_ratio:
        print(f"Increase left upper arm circumference to {ideal_upper_arm_ratio:.2f} cm")
    if right_upper_arm_circumference < ideal_upper_arm_ratio:
        print(f"Increase right upper arm circumference to {ideal_upper_arm_ratio:.2f} cm")
    if chest_circumference < ideal_chest_ratio:
        print(f"Increase chest circumference to {ideal_chest_ratio:.2f} cm")
    if waist_circumference < ideal_waist_ratio:
        print(f"Increase waist circumference to {ideal_waist_ratio:.2f} cm")
    if hips_circumference < ideal_hips_ratio:
        print(f"Increase hips circumference to {ideal_hips_ratio:.2f} cm")
    if left_thigh_circumference < ideal_thigh_ratio:
        print(f"Increase left thigh circumference to {ideal_thigh_ratio:.2f} cm")
    if right_thigh_circumference < ideal_thigh_ratio:
        print(f"Increase right thigh circumference to {ideal_thigh_ratio:.2f} cm")
    if left_calf_circumference < ideal_calf_ratio:
        print(f"Increase left calf circumference to {ideal_calf_ratio:.2f} cm")
    if right_calf_circumference < ideal_calf_ratio:
        print(f"Increase right calf circumference to {ideal_calf_ratio:.2f} cm")
    if weight < ideal_weight:
        print(f"Increase weight to {ideal_weight:.2f} kg")


if __name__ == "__main__":
    main()
