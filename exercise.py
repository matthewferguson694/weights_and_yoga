import os
from datetime import date


def get_current_stage(start_date, today):
    strength_duration = 12  # weeks
    transition_duration = 2  # weeks
    hypertrophy_duration = 12  # weeks
    deload_duration = 1  # week
    cycle_duration = strength_duration + transition_duration + hypertrophy_duration + transition_duration + deload_duration

    # Calculate the number of weeks since the start date
    weeks_elapsed = (today - start_date).days // 7

    # Calculate the current week within the training cycle
    current_cycle_week = weeks_elapsed % cycle_duration

    if 0 <= current_cycle_week < strength_duration:
        return "strength"
    elif strength_duration <= current_cycle_week < strength_duration + transition_duration:
        return "transition"
    elif strength_duration + transition_duration <= current_cycle_week < strength_duration + transition_duration + hypertrophy_duration:
        return "hypertrophy"
    else:
        return "transition"


# Define the paths to the scripts
strength_script_path = "/Users/matthewferguson/Developer/weights_and_yoga/startingstrength.py"
hypertrophy_script_path = "/Users/matthewferguson/Developer/weights_and_yoga/path/to/hypertrophy.py"

# Define the start date and today's date
start_date = date(2023, 1, 1)
today = date.today()

# Call the get_current_stage function and get the current stage
current_stage = get_current_stage(start_date, today)

# Determine which script to open based on the current stage
if current_stage == "strength":
    script_path = strength_script_path
elif current_stage == "hypertrophy":
    script_path = hypertrophy_script_path
else:
    print("Unknown stage.")
    exit()

# Open the script using the default program associated with its file type
os.system(f"python3 {script_path}")
